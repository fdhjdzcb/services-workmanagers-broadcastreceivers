package com.example.servwmanbrrec.models

data class CatFact(
    val fact: String,
    val length: Int
)
