package com.example.servwmanbrrec

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkInfo
import androidx.work.WorkManager
import com.example.servwmanbrrec.models.CatFact
import com.example.servwmanbrrec.viewmodel.MainViewModel
import com.example.servwmanbrrec.workers.CatFactWorker

import com.google.gson.Gson

class MainActivity : ComponentActivity() {
    private val viewModel: MainViewModel by viewModels()
    private lateinit var receiver: BroadcastReceiver
    private lateinit var observer: Observer<WorkInfo>
    private lateinit var workInfo: LiveData<WorkInfo>
    private lateinit var navController: NavHostController
    private val request = OneTimeWorkRequest.Builder(CatFactWorker::class.java).build()


    @SuppressLint("InlinedApi")
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            navController = rememberNavController()
            MainScreen(navController, viewModel, request)
        }

        val catFactsReceiver = createCatFactsReceiver()
        registerReceiver(catFactsReceiver, IntentFilter("com.example.twoscreenapp"), Context.RECEIVER_EXPORTED)

        val workInfoObserver = createWorkInfoObserver()
        val workManager = WorkManager.getInstance(this)
        val workInfo = workManager.getWorkInfoByIdLiveData(request.id)
        workInfo.observe(this, workInfoObserver)
    }

    private fun createCatFactsReceiver(): BroadcastReceiver {
        return object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                intent?.getStringExtra("catFacts")?.let { catFactsJson ->
                    val catFacts = Gson().fromJson(catFactsJson, Array<CatFact>::class.java).toList()
                    viewModel.updateCatFacts(catFacts)
                    navController.navigate("second")
                }
            }
        }
    }

    private fun createWorkInfoObserver(): Observer<WorkInfo> {
        return Observer { workInfo ->
            if (workInfo.state == WorkInfo.State.SUCCEEDED) {
                workInfo.outputData.getString("catFacts")?.let { catFactsJson ->
                    val catFacts = Gson().fromJson(catFactsJson, Array<CatFact>::class.java).toList()
                    viewModel.updateCatFacts(catFacts)
                    navController.navigate("second")
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(receiver)
        workInfo.removeObserver(observer)
    }
}


@Composable
fun ChooseScreen(viewModel: MainViewModel, request: OneTimeWorkRequest) {
    val context = LocalContext.current
    var isServiceLoading by remember { mutableStateOf(false) }
    var isWMLoading by remember { mutableStateOf(false) }
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = stringResource(R.string.select_text),
            fontSize = 28.sp,
            textAlign = TextAlign.Center,
        )
        Spacer(modifier = Modifier.height(16.dp))
        Button(
            onClick = {
                isServiceLoading = true
                viewModel.startService(context)
            },
            modifier = Modifier
                .width(250.dp)
                .height(60.dp)
        ) {
            Text(
                text = stringResource(R.string.service_btn),
                fontSize = 24.sp
            )
            Spacer(modifier = Modifier.width(8.dp))
            if (isServiceLoading)
                CircularProgressIndicator(
                    color = Color.White,
                    modifier = Modifier.size(16.dp)
                )
        }
        Spacer(modifier = Modifier.height(16.dp))
        Button(
            onClick = {
                isWMLoading = true
                viewModel.startWorkManager(context, request)
            },
            modifier = Modifier
                .width(250.dp)
                .height(60.dp)
        ) {
            Text(
                text = stringResource(R.string.workmanager_btn),
                fontSize = 24.sp
            )
            Spacer(modifier = Modifier.width(8.dp))
            if (isWMLoading)
                CircularProgressIndicator(
                    color = Color.White,
                    modifier = Modifier.size(16.dp)
                )
        }
        Image(
            painter = painterResource(R.drawable.cat_image), // Замените на ресурс вашего изображения кота
            contentDescription = "Cat Image",
            modifier = Modifier.size(350.dp) // Установите размер изображения по вашему усмотрению
        )
    }
}


@Composable
fun SecondScreen(viewModel: MainViewModel) {
    val catFacts by viewModel.catFacts.observeAsState(initial = emptyList())
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = stringResource(R.string.cats_fact_label),
            fontSize = 30.sp,
            modifier = Modifier
                .background(Color(0xFF882DEB))
                .fillMaxWidth()
                .padding(12.dp),
            textAlign = TextAlign.Center,
            color = Color.White
        )
        Spacer(modifier = Modifier.height(12.dp))
        LazyColumn {
            itemsIndexed(catFacts) { index, catFact ->
                Card(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(8.dp),
                ) {
                    Row(verticalAlignment = Alignment.CenterVertically) {
                        Text(
                            text = "${index + 1}.",
                            modifier = Modifier.padding(16.dp),
                        )
                        Text(
                            text = catFact.fact,
                            modifier = Modifier.padding(16.dp),
                            fontSize = 18.sp
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun MainScreen(
    navController: NavHostController,
    viewModel: MainViewModel,
    request: OneTimeWorkRequest
) {
    NavHost(navController = navController, startDestination = "choose") {
        composable("choose") {
            ChooseScreen(viewModel = viewModel, request)
        }
        composable("second") {
            SecondScreen(viewModel = viewModel)
        }
    }
}