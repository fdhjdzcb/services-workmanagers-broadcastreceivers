package com.example.servwmanbrrec.services

import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.example.servwmanbrrec.models.CatFact
import com.google.gson.Gson
import com.google.gson.JsonObject
import java.net.HttpURLConnection
import java.net.URL

class CatFactService : Service() {
    private val apiUrl = "https://catfact.ninja/facts?limit=25"
    private val broadcastIntent = Intent("com.example.twoscreenapp")

    private fun getFacts() {
        Thread {
            try {
                val catFacts = fetchCatFacts()
                broadcastIntent.putExtra("catFacts", Gson().toJson(catFacts))
                sendBroadcast(broadcastIntent)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }.start()
    }

    private fun fetchCatFacts(): List<CatFact> {
        var catFacts: List<CatFact> = emptyList()
        try {
            val url = URL(apiUrl)
            val connection = url.openConnection() as HttpURLConnection
            connection.requestMethod = "GET"
            connection.connectTimeout = 10000
            val response = connection.inputStream.bufferedReader().readText()
            connection.disconnect()
            val json = Gson().fromJson(response, JsonObject::class.java) // Используем JsonObject вместо Map
            val data = json.getAsJsonArray("data")
            catFacts = data.map {
                val factObject = it.asJsonObject
                CatFact(
                    fact = factObject.get("fact").asString,
                    length = factObject.get("length").asInt
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return catFacts
    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Thread.sleep(2500)
        getFacts()
        return START_NOT_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }
}