package com.example.servwmanbrrec.ui.theme

import androidx.compose.ui.graphics.Color

val Green80 = Color(0xFFFFFFFF)
val GreenGrey80 = Color(0xFFA0F6AD)
val Cyan80 = Color(0xFF85D993)

val Green40 = Color(0xFF136D34)
val GreenGrey40 = Color(0xFF005323)
val Cyan40 = Color(0xFF296D59)


